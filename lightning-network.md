## Lightning Network

A Lightning Network (LN) channel is a contract with escrow; a hashed-timelock agreement. The contract determines who has control of what portion. A transaction within a channel is you signing over control of a portion of your BTC held within escrow over to the other person. 

### General overviews

* [Bitcoin wiki: Lightning Network](https://en.bitcoin.it/wiki/Lightning_Network)
* [What are hashed timelock contracts (HTLCs)?](https://hackernoon.com/what-are-hashed-timelock-contracts-htlcs-application-in-lightning-network-payment-channels-14437eeb9345)
* [lightning.network](http://lightning.network/)
* [Wikipedia: Lightning Network](https://en.wikipedia.org/wiki/Lightning_Network)
* [List of LN projects](https://github.com/bcongdon/awesome-lightning-network)
* [Blockstream 7 Lightning apps](https://btcmanager.com/unveiling-blockstreams-7-lightning-apps-lapps/)

## Top 3 LN implementations of BOLTs under active development: lnd, c-lightning, Eclair

* [lnd (Lightning Network Daemon)](https://github.com/lightningnetwork/lnd)

  There are two RPC interfaces: an HTTP REST API and a [gRPC](https://grpc.io/) service. These can change.

  [Docker compose](https://github.com/lightningnetwork/lnd/tree/master/docker) available.
  Install guides:

  * [Run your own mainnet Lightning Node](https://medium.com/@dougvk/run-your-own-mainnet-lightning-node-2d2eab628a8b) 20160121

* [c-lightning](https://github.com/ElementsProject/lightning) from [The Elements Project](https://elementsproject.org/)
  This is a modular, performance and TOR focused implementation. There is a c-lightning RPC port and a JSON API proxy which supports only POST requests. Backward compatibility will be maintained.
  [Install instructions for Android, Raspberry, Linux, NixOS, FreeBSD, MacOS](https://github.com/ElementsProject/lightning/blob/master/doc/INSTALL.md)

  ```
  sudo apt-get update
  sudo apt-get install -y \
    autoconf automake build-essential git libtool libgmp-dev \
    libsqlite3-dev python python3 net-tools zlib1g-dev
  git clone https://github.com/ElementsProject/lightning.git
  cd lightning
  ./configure
  make
  ```

  [c-lightning Docker image](https://hub.docker.com/r/elementsproject/lightningd/) available.

  [Information on the last c-lightning 0.6 release](https://blockstream.com/2018/06/25/c-lightning-06.html) 20180625

  

* [Eclair](https://github.com/ACINQ/eclair), French for Lightning, from [ACINQ](https://acinq.co) based in Paris.

  This is a Scala/JVM based implementation and has a [JSON-RPC API](https://github.com/ACINQ/eclair/wiki/API). Eclair depends on a synchronized, segwit, zeromq, wallet-enabled, non-pruning, tx-indexing [Bitcoin Core](https://github.com/bitcoin/bitcoin) node. It has Lightning Network ['data loss protection'](https://medium.com/@ACINQ/adding-data-loss-protection-to-eclair-598c62494096). It can be started with or without GUI and runs wherever a JVM runs; Windows, Linux, FreeBSD and MacOS. 
  [The JSON-RPC API](https://github.com/ACINQ/eclair) is documented.

  There is an Eclair mobile version for Android which provides a Lightning-ready Bitcoin wallet on a phone and imports existing wallets. The Lightning channel is outbound only, it cannot receive. [LN-Zap](https://github.com/LN-Zap) is a popular Electron based desktop LN wallet.

  [ACINQ](https://acinq.co) provides their own hosted API, [Strike](https://strike.acinq.co/), for merchants to use with a 1% transaction fee, it has a dashboard and powers their [Starblocks example webstore](https://starblocks.acinq.co/). But as [BTCPay](https://github.com/btcpayserver) integrated LN (you can choose between lnd and c-lightning) one can self-host an LN payment processor with Docker or an easy to use Azure template. BitPay (which also has an office in Utrecht) does not currently support LN. The Elements Project elements and Blockstream LApps can also be used to assemble a self-hosted payment processor based on c-lightning.

  [Eclair Dockerfile available](https://hub.docker.com/r/acinq/eclair/)

  - [Eclair setup](https://github.com/ACINQ/eclair/wiki/Install), basically running a JRE 1.8 jar, it's maven dependecies with config connecting to an existing Bitcoin Core node, for example a [bitcoin-core docker image](https://hub.docker.com/r/ruimarinho/bitcoin-core/).

  - [Eclair windows setup](https://medium.com/@frederikro/setting-up-lightning-with-eclair-from-acinq-on-bitcoin-testnet-on-a-windows-machine-cbbbc41a399e)

  